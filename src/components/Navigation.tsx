import { MdDonutLarge } from "react-icons/md";
import { BiBorderAll } from "react-icons/bi";
import { NavLink } from "react-router-dom";
import { ActivePageType } from "../models/ActivePageType";
//import { Link } from "react-router-dom";

export interface NavigationProps {
  activePageType: ActivePageType;
}

const Navigation: React.FC<NavigationProps> = ({ activePageType }) => {
  return (
    <nav className="col-1">
      <article className="sidebar">
        <NavLink
          to={"/login"}
          className={
            "navigationIcon " +
            (activePageType === ActivePageType.Login ? "active" : "notActive")
          }
        >
          <div className="">
            <MdDonutLarge size="4em" />
          </div>
          Login
        </NavLink>
        <NavLink
          to="/"
          className={
            "navigationIcon " +
            (activePageType === ActivePageType.Home ? "active" : "notActive")
          }
        >
          <div className="">
            <BiBorderAll size="4em" />
          </div>
          Home
        </NavLink>
        <NavLink
          to="/blogs"
          className={
            "navigationIcon " +
            (activePageType === ActivePageType.Blogs ? "active" : "notActive")
          }
        >
          <div className="">
            <MdDonutLarge size="4em" />
          </div>
          BLogs
        </NavLink>
        <NavLink
          to="/test"
          className={
            "navigationIcon " +
            (activePageType === ActivePageType.Nix ? "active" : "notActive")
          }
        >
          <div className="">
            <BiBorderAll size="4em" />
          </div>
          Nix
        </NavLink>
      </article>
    </nav>
  );
};

export default Navigation;
