import MainLayout from "../Pages/MainLayout";
import { ActivePageType } from "../models/ActivePageType";


export interface LoginPageProps {}

const LoginPage: React.FC<LoginPageProps> = () => {
  return <MainLayout ActivePageType={ActivePageType.Login} children={<div>blaablaa</div>} />;
};

export default LoginPage;
