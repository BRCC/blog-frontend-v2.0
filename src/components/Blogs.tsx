import * as React from "react";
import TwoColumnPage from "../Pages/TwoColumnPage";
import { BlogEntries as BlogEntriesModel } from "../models/BlogEntries";
import { Author as AuthorModel } from "../models/Author";
import { NavLink } from "react-router-dom";
import { ActivePageType } from "../models/ActivePageType";

export interface BlogsProps {}

const Blogs: React.FC<BlogsProps> = ({}: BlogsProps) => {
  const [blogs, setBlogs] = React.useState<BlogEntriesModel[]>([]);
  const [viewedBlogs, setViewedBlogs] = React.useState<BlogEntriesModel[]>([]);
  const [blog, setBlog] = React.useState<BlogEntriesModel>();
  const [author, setAuthor] = React.useState<AuthorModel[]>([]);

  React.useEffect(() => {
    fetch("http://localhost:3001/blog/")
      .then((res) => res.json())
      .then((result) => {
        setBlogs(result);
        setViewedBlogs(result.slice(0, 10));
        setAuthor(result.author);
      });
  }, []);

  const buttonHandler = (
    event: React.MouseEvent<HTMLElement>,
    blogItem: any
  ) => {
    setBlog(blogItem);
  };

  const changePage = (event: React.MouseEvent<HTMLElement>) => {
    console.log(blogs);
    let number: number = parseInt((event.target as HTMLButtonElement).id);
    number = (number - 1) * 10;
    console.log(number);
    blogs.slice(number, number + 10);
    console.log(blogs.slice(number, number + 10));
    setViewedBlogs(blogs.slice(number, number + 10));
    setBlog(undefined);
  };

  const blogsCount: Number = Math.ceil(blogs.length / 10);

  const items = [];

  for (let i = 1; i <= blogsCount; i++) {
    items.push(
      <button id={i.toString()} onClick={(event) => changePage(event)}>
        {i}
      </button>
    );
  }

  return (
    <TwoColumnPage
      ActivePageType={ActivePageType.Blogs}
      mainElement={
        <div
          className="blogEntries"
          onClick={() => console.log("setBlog(undefined)")}
        >
          {viewedBlogs.map((blogItem) => (
            <>
              <article
                className="blogEntry"
                onClick={(event) => buttonHandler(event, blogItem)}
              >
                <p className="title">{blogItem.title}</p>
                <h3 className="author">{blogItem.author.username}</h3>
              </article>
            </>
          ))}
          <div>{items}</div>
        </div>
      }
      sidebarElement={
        <>
          {typeof blog != "undefined" && (
            <article className="blogPreview">
              <div className="data">
                <h2 className="blogPreviewTitle">{blog?.title}</h2>
                {blog.imageUrl && (
                  <img className="blogImagePreview" src={blog.imageUrl}></img>
                )}
              </div>
              <div className="information">
                <h2>Information:</h2>
                <div className="informationFlex">
                  <p>Author:</p>
                  <p>{blog.author.username}</p>
                </div>
                <div className="informationFlex">
                  <p>Category:</p>
                  <p>{blog.category}</p>
                </div>
                <div className="informationFlex">
                  <p>Created:</p>
                  <p>{blog.createdAt}</p>
                </div>
              </div>
              <div className="openButton">
                <NavLink
                  to="/blogEntry"
                  state={{ blog: blog }}
                  className={({ isActive }) =>
                    "navigationIcon " + (isActive ? "active" : "notActive")
                  }
                >
                  <button>Öffnen</button>
                </NavLink>
              </div>
            </article>
          )}
        </>
      }
    />
  );
};

export default Blogs;
