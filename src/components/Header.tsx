import React from "react";

export interface HeaderProps {}

const Header: React.FC<HeaderProps> = () => {
  return (
    <>
      <header>Header</header>
    </>
  );
};

export default Header;
