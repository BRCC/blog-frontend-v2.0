import * as React from "react";
import parse from "react-html-parser";

import TwoColumnPage from "../Pages/TwoColumnPage";
import { BlogEntries as BlogEntriesModel } from "../models/BlogEntries";
import { Author as AuthorModel } from "../models/Author";
import MainLayout from "../Pages/MainLayout";
import { useLocation } from "react-router";
import { NavLink } from "react-router-dom";
import { ActivePageType } from "../models/ActivePageType";

interface LocationState {
  state: {
    blog: BlogEntriesModel;
  };
}

export interface BlogEntryProps {}

const BlogEntry: React.FC<BlogEntryProps> = ({}: BlogEntryProps) => {
  const location = useLocation();

  const blog = (location as LocationState)?.state.blog;

  return (
    <MainLayout
      ActivePageType={ActivePageType.Blogs}
      children={
        <>
          <div className="blogEntry">
            <div className="content">
              <h1>{blog.title}</h1>
              <p>{blog.content}</p>
              <p>{parse(blog.author.username)}</p>
            </div>
            <div className="button">
              <NavLink to="/blogs">
                <button>Zurück</button>
              </NavLink>
            </div>
          </div>
        </>
      }
    />
  );
};

export default BlogEntry;
