import React from "react";
import logo from "./logo.svg";
import "./App.css";
import LoginPage from "./components/LoginPage";
import HomePage from "./components/HomePage";
import BlogEntry from "./components/BlogEntry";
import Blogs from "./components/Blogs";
import { BrowserRouter, Route, Routes } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <div className="container">
        <BrowserRouter>
          <Routes>
            <Route path="/login" element={<LoginPage />} />
            <Route path="/" element={<HomePage />} />
            <Route path="/blogs" element={<Blogs />} />
            <Route path="/blogEntry" element={<BlogEntry />} />
          </Routes>
        </BrowserRouter>
      </div>
      {/* <Navigation />
      <Header /> */}
    </div>
  );
}

export default App;
