export enum ActivePageType {
  Login = 0,
  Home = 1,
  Blogs = 2,
  Nix = 3,
}