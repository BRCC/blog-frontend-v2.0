import { Author as AuthorModel } from "./Author";

export class BlogEntries {
  _id!: string;
  author = new AuthorModel();
  category!: string;
  comments!: string[];
  content!: string;
  createdAt!: string;
  imageUrl!: string;
  title!: string;
  updatedAt!: string;

  // constructor(id: string, name: string, isFolder: boolean) {
  //   this.id = id;
  //   this.name = name;
  //   this.isFolder = isFolder;
  // }
}
