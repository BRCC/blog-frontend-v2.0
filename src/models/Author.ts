export class Author {
  _id!: string;
  email!: string;
  imageUrl!: string;
  isDeleted!: boolean;
  password!: string;
  updatedAt!: string;
  username!: string;

  // constructor(id: string, name: string, isFolder: boolean) {
  //   this.id = id;
  //   this.name = name;
  //   this.isFolder = isFolder;
  // }
}
