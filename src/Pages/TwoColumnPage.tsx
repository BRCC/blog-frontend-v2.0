import { MdDonutLarge } from "react-icons/md";
import { BiBorderAll } from "react-icons/bi";
import { NavLink } from "react-router-dom";
import MainLayout from "./MainLayout";
import { ActivePageType } from "../models/ActivePageType";

//import { Link } from "react-router-dom";

export interface TwoColumnPageProps {
  mainElement?: React.ReactNode;
  sidebarElement?: React.ReactNode;
  ActivePageType: ActivePageType
}

const TwoColumnPage: React.FC<TwoColumnPageProps> = ({
  mainElement,
  sidebarElement,
  ActivePageType
}) => {
  return (
    <MainLayout ActivePageType={ActivePageType}>
      <main className="mainElement">
        {/* Primary column */}
        <section aria-labelledby="primary-heading" className="test">
          {mainElement}
        </section>
      </main>

      <aside className="sidebarElement">{sidebarElement}</aside>
    </MainLayout>
  );
};

export default TwoColumnPage;
