import * as React from "react";
import Header from "../components/Header";
import Navigation from "../components/Navigation";
import { ActivePageType } from "../models/ActivePageType";

//import { Link } from "react-router-dom";

export interface MainLayoutProps {
  children: React.ReactNode;
  ActivePageType: ActivePageType
}

const MainLayout: React.FC<MainLayoutProps> = ({
  children,
  ActivePageType
}: MainLayoutProps) => {
  return (
    <>
      <Navigation activePageType={ActivePageType}/>
      <div className="col-2">
        <Header />
        <main className="content">
          <article className="mainContent">{children}</article>
        </main>
        <footer>Footer</footer>
      </div>
    </>
  );
};

export default MainLayout;
